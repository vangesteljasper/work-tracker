import localforage from "localforage";

export const save = (key, data) => {
  return localforage.setItem(key, data);
};

export const get = callback => {
  return localforage.iterate(callback);
};
